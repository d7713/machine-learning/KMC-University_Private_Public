# Universities Private / Public (K Means Clustering)

### By: Andrew Wairegi

## Description
To determine whether a university is public / private, given the fields. This will allow
us to determine whether a university can be classified given their details. If the
model is successfull we will be able classify different datasets, even though they have overlapping features.
However, I don't believe the model will do well due to their overlapping features. 

[Open notebook]

## Setup/installation instructions
1. Find a local folder on your computer
2. Set it up as an empty repository (git init)
3. Clone this repository there (git clone https://...)
4. Upload the notebook to google drive
5. Open it
6. Upload the data file to google collab (the file upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs.

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

### License
Copyright (c) 2022 **Andrew Wairegi**

[Open notebook]: https://gitlab.com/d7713/machine-learning/KMC-University_Private_Public/-/blob/main/Universities_Private_Public_(K_means_clustering).ipynb
